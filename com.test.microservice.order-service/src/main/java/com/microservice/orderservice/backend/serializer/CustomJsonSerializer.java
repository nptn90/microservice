package com.microservice.orderservice.backend.serializer;

import com.microservice.orderservice.backend.entities.Event;
import org.springframework.kafka.support.serializer.JsonSerializer;

public class CustomJsonSerializer<T extends Event> extends JsonSerializer<T> {
	//Do nothing
}
