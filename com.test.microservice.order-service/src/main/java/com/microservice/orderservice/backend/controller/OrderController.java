package com.microservice.orderservice.backend.controller;

import com.microservice.orderservice.backend.dto.AppProperties;
import com.microservice.orderservice.backend.dto.OrderRequest;
import com.microservice.orderservice.backend.dto.OrderResponse;
import com.microservice.orderservice.backend.dto.OrderResponse.Data;
import com.microservice.orderservice.backend.service.OrderService;
import com.microservice.orderservice.backend.service.TestBeanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController(value = "/order")
public class OrderController {

  @Autowired
  private OrderService orderService;

  @Autowired
  private TestBeanService testBeanService;

  @PostMapping(value = "/order-product")
  public OrderResponse orderProduct(@Validated @RequestBody OrderRequest orderProductRequest) {
    String response = orderService.orderProduct(orderProductRequest);
    return new OrderResponse(new Data(testBeanService.getValue("appName")));
  }
}
