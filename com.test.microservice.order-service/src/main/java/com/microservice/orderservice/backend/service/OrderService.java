package com.microservice.orderservice.backend.service;

import com.microservice.orderservice.backend.dto.OrderRequest;
import com.microservice.orderservice.backend.entities.OrderEvent;
import com.microservice.productservice.backend.proto.OrderProductRequest;
import com.microservice.productservice.backend.proto.OrderProductResponse;
import com.microservice.productservice.backend.proto.OrderProductServiceGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderService {

  @Autowired
  EventPublisher eventPublisher;

  public String orderProduct(OrderRequest order) {
    eventPublisher.publishEvent(OrderEvent.builder()
        .orderId(order.getProductId())
        .message("order sucess")
        .dateTime(new Date())
        .build()
    );
    ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 8088)
        .usePlaintext()
        .build();

    OrderProductServiceGrpc.OrderProductServiceBlockingStub stub
        = OrderProductServiceGrpc.newBlockingStub(channel);

    OrderProductResponse orderResponse = stub.order(OrderProductRequest.newBuilder()
        .setProductId(order.getProductId())
        .setQuantity(order.getQuantity())
        .build());

    channel.shutdown();
    return orderResponse.getMessage();
  }
}
