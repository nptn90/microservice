package com.microservice.orderservice.backend.service;

import java.util.Map;
import lombok.Builder;
import lombok.Getter;
import org.springframework.beans.factory.DisposableBean;

@Builder
@Getter
public class TestBeanService implements DisposableBean {

  private Map<String, String> mapValue;
  private OrderService orderService;

  public String getValue(String key) {
    return mapValue.getOrDefault(key, "");
  }

  @Override
  public void destroy() throws Exception {
    System.out.println("Bean was destroyed");
  }
}
