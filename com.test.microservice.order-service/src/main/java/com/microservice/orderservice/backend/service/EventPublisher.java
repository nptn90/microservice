package com.microservice.orderservice.backend.service;

import com.microservice.orderservice.backend.constant.StringConstant;
import com.microservice.orderservice.backend.entities.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class EventPublisher {

  @Autowired
  private KafkaTemplate<String, Event> kafkaTemplate;

  public <T extends Event> void publishEvent(T event) {
    kafkaTemplate.send(StringConstant.ORDER_PRODUCT, event);
  }
}
