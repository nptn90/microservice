package com.microservice.orderservice.backend.entities;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@Getter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class OrderEvent extends Event {

  private String orderId;
  private Date dateTime;

  @Builder
  public OrderEvent(String message, String orderId, Date dateTime) {
    super(message);
    this.orderId = orderId;
    this.dateTime = dateTime;
  }
}
