package com.microservice.orderservice.backend.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class BaseErrorModel {
  private String code;
  private String message;
}
