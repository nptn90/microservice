package com.microservice.orderservice.backend.dto;

import com.microservice.orderservice.backend.dto.OrderResponse.Data;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode(callSuper = false)
public class OrderResponse extends BaseResponseModel<Data, BaseErrorModel> {

  public OrderResponse(Data data) {
    super(data);
  }

  @Builder
  @AllArgsConstructor
  @Getter
  public static class Data implements Serializable {

    private static final long serialVersionUID = 6085032073499207988L;
    private final String message;
  }
}
