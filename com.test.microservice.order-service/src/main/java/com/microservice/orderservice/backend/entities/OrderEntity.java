package com.microservice.orderservice.backend.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import lombok.Getter;
import org.hibernate.annotations.GenericGenerator;

@Entity(name = "order_product")
@Getter
public class OrderEntity {

  @Id
  @GeneratedValue(generator = "uuid2")
  @GenericGenerator(name = "uuid2", strategy = "uuid2")
  @NotEmpty
  private String id;

  @Column(name = "product_id")
  private String productId;

  @Column(name = "quantity")
  private int quantity;
}
