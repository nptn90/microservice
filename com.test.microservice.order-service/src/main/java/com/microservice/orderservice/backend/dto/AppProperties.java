package com.microservice.orderservice.backend.dto;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.validation.annotation.Validated;

@ConfigurationProperties(prefix = "app")
@ConstructorBinding
@Getter
@Validated
public class AppProperties {

  private String appName;
  private int numberOfWeek;

  @Autowired
  public AppProperties(String appName, int numberOfWeek) {
    System.out.println("createing app properties");
    this.appName = appName;
    this.numberOfWeek = numberOfWeek;
  }
}
