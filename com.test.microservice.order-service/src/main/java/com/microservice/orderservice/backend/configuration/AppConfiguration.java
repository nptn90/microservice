package com.microservice.orderservice.backend.configuration;

import com.microservice.orderservice.backend.dto.AppProperties;
import com.microservice.orderservice.backend.service.OrderService;
import com.microservice.orderservice.backend.service.TestBeanService;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.web.context.WebApplicationContext;

@Configuration
public class AppConfiguration {

  @Bean
  public TestBeanService createTestService(AppProperties appProperties, OrderService orderService) {
    System.out.println("Creating bean test service");
    Map<String, String> mapProperties = new HashMap<>();
    mapProperties.put("appName", appProperties.getAppName());
    return TestBeanService.builder()
        .mapValue(mapProperties)
        .orderService(orderService)
        .build();
  }


}
