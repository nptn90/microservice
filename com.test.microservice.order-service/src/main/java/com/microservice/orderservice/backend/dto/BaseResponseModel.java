package com.microservice.orderservice.backend.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@AllArgsConstructor
@Getter
public class BaseResponseModel<D, E> implements Serializable {

  private static final long serialVersionUID = -4260969347274231433L;
  private final String apiVersion = "version 1.0";
  private String requestId;
  private final boolean success;
  private final String code;
  private final D data;

  @JsonInclude(Include.NON_NULL)
  private E errors;

  public BaseResponseModel(HttpStatus http, D data) {
    this.success = true;
    this.data = data;
    this.code = http.name();
  }

  public BaseResponseModel(D data) {
    this(HttpStatus.OK, data);
  }
}
