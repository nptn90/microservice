package com.microservice.productservice.backend.proto;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.25.0)",
    comments = "Source: ProductService.proto")
public final class OrderProductServiceGrpc {

  private OrderProductServiceGrpc() {}

  public static final String SERVICE_NAME = "com.microservice.productservice.backend.proto.OrderProductService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.microservice.productservice.backend.proto.OrderProductRequest,
      com.microservice.productservice.backend.proto.OrderProductResponse> getOrderMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "order",
      requestType = com.microservice.productservice.backend.proto.OrderProductRequest.class,
      responseType = com.microservice.productservice.backend.proto.OrderProductResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.microservice.productservice.backend.proto.OrderProductRequest,
      com.microservice.productservice.backend.proto.OrderProductResponse> getOrderMethod() {
    io.grpc.MethodDescriptor<com.microservice.productservice.backend.proto.OrderProductRequest, com.microservice.productservice.backend.proto.OrderProductResponse> getOrderMethod;
    if ((getOrderMethod = OrderProductServiceGrpc.getOrderMethod) == null) {
      synchronized (OrderProductServiceGrpc.class) {
        if ((getOrderMethod = OrderProductServiceGrpc.getOrderMethod) == null) {
          OrderProductServiceGrpc.getOrderMethod = getOrderMethod =
              io.grpc.MethodDescriptor.<com.microservice.productservice.backend.proto.OrderProductRequest, com.microservice.productservice.backend.proto.OrderProductResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "order"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.microservice.productservice.backend.proto.OrderProductRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.microservice.productservice.backend.proto.OrderProductResponse.getDefaultInstance()))
              .setSchemaDescriptor(new OrderProductServiceMethodDescriptorSupplier("order"))
              .build();
        }
      }
    }
    return getOrderMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static OrderProductServiceStub newStub(io.grpc.Channel channel) {
    return new OrderProductServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static OrderProductServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new OrderProductServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static OrderProductServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new OrderProductServiceFutureStub(channel);
  }

  /**
   */
  public static abstract class OrderProductServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void order(com.microservice.productservice.backend.proto.OrderProductRequest request,
        io.grpc.stub.StreamObserver<com.microservice.productservice.backend.proto.OrderProductResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getOrderMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getOrderMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.microservice.productservice.backend.proto.OrderProductRequest,
                com.microservice.productservice.backend.proto.OrderProductResponse>(
                  this, METHODID_ORDER)))
          .build();
    }
  }

  /**
   */
  public static final class OrderProductServiceStub extends io.grpc.stub.AbstractStub<OrderProductServiceStub> {
    private OrderProductServiceStub(io.grpc.Channel channel) {
      super(channel);
    }

    private OrderProductServiceStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected OrderProductServiceStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new OrderProductServiceStub(channel, callOptions);
    }

    /**
     */
    public void order(com.microservice.productservice.backend.proto.OrderProductRequest request,
        io.grpc.stub.StreamObserver<com.microservice.productservice.backend.proto.OrderProductResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getOrderMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class OrderProductServiceBlockingStub extends io.grpc.stub.AbstractStub<OrderProductServiceBlockingStub> {
    private OrderProductServiceBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private OrderProductServiceBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected OrderProductServiceBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new OrderProductServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public com.microservice.productservice.backend.proto.OrderProductResponse order(com.microservice.productservice.backend.proto.OrderProductRequest request) {
      return blockingUnaryCall(
          getChannel(), getOrderMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class OrderProductServiceFutureStub extends io.grpc.stub.AbstractStub<OrderProductServiceFutureStub> {
    private OrderProductServiceFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private OrderProductServiceFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected OrderProductServiceFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new OrderProductServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.microservice.productservice.backend.proto.OrderProductResponse> order(
        com.microservice.productservice.backend.proto.OrderProductRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getOrderMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_ORDER = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final OrderProductServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(OrderProductServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_ORDER:
          serviceImpl.order((com.microservice.productservice.backend.proto.OrderProductRequest) request,
              (io.grpc.stub.StreamObserver<com.microservice.productservice.backend.proto.OrderProductResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class OrderProductServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    OrderProductServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.microservice.productservice.backend.proto.ProductService.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("OrderProductService");
    }
  }

  private static final class OrderProductServiceFileDescriptorSupplier
      extends OrderProductServiceBaseDescriptorSupplier {
    OrderProductServiceFileDescriptorSupplier() {}
  }

  private static final class OrderProductServiceMethodDescriptorSupplier
      extends OrderProductServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    OrderProductServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (OrderProductServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new OrderProductServiceFileDescriptorSupplier())
              .addMethod(getOrderMethod())
              .build();
        }
      }
    }
    return result;
  }
}
