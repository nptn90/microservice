package com.microservice.audit.backend.configuration;

import com.microservice.audit.backend.dto.OrderEvent;
import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import com.microservice.audit.backend.common.StringConstant;
import com.microservice.audit.backend.dto.UpdateEvent;

@EnableKafka
@Configuration
public class KafkaConfiguration {

	public ConsumerFactory<String,UpdateEvent> consumerFactory() {
        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,  "localhost:9092");
        props.put(ConsumerConfig.GROUP_ID_CONFIG, StringConstant.PRODUCT);
        JsonDeserializer<UpdateEvent> valueDeserializer = new JsonDeserializer<>(UpdateEvent.class);
        Map<String, String> map = new HashMap<>();
        map.put(JsonDeserializer.TRUSTED_PACKAGES, "*");
        valueDeserializer.configure(map, true);
        return new DefaultKafkaConsumerFactory<>(props, new StringDeserializer(), valueDeserializer);
	}

	@Bean
	public ConcurrentKafkaListenerContainerFactory<String, UpdateEvent> productKafkaListenerContainerFactory() {

		ConcurrentKafkaListenerContainerFactory<String, UpdateEvent> factory = new ConcurrentKafkaListenerContainerFactory<>();
		factory.setConsumerFactory(consumerFactory());
		return factory;
	}

  public ConsumerFactory<String, OrderEvent> orderConsumerFactory() {
    Map<String, Object> props = new HashMap<>();
    props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,  "localhost:9092");
    props.put(ConsumerConfig.GROUP_ID_CONFIG, StringConstant.PRODUCT);
    JsonDeserializer<OrderEvent> valueDeserializer = new JsonDeserializer<>(OrderEvent.class);
    Map<String, String> map = new HashMap<>();
    map.put(JsonDeserializer.TRUSTED_PACKAGES, "*");
    valueDeserializer.configure(map, true);
    return new DefaultKafkaConsumerFactory<>(props, new StringDeserializer(), valueDeserializer);
  }

  @Bean
  public ConcurrentKafkaListenerContainerFactory<String, OrderEvent> orderKafkaListenerContainerFactory() {

    ConcurrentKafkaListenerContainerFactory<String, OrderEvent> factory = new ConcurrentKafkaListenerContainerFactory<>();
    factory.setConsumerFactory(orderConsumerFactory());
    return factory;
  }
}
