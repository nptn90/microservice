package com.microservice.audit.backend.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.microservice.audit.backend.dto.Event;
import com.microservice.audit.backend.dto.OrderEvent;
import org.apache.kafka.common.serialization.Deserializer;
import org.springframework.stereotype.Service;

@Service
public class CustomJsonDeserialzer implements Deserializer<Event> {

  @Override
  public Event deserialize(String topic, byte[] data) {
    ObjectMapper mapper = new ObjectMapper();
    Event event = null;
    try {
      event = mapper.readValue(data, Event.class);
    } catch (Exception e) {

      e.printStackTrace();
    }
    return new OrderEvent();
  }
}
