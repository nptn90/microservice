package com.microservice.audit.backend.common;

public final class StringConstant {
	public static final String UPDATE_PRODUCT = "update_product";
	public static final String PRODUCT = "product";

	public static final String ORDER_PRODUCT = "order_product";
	public static final String ORDER = "order";
}
