package com.microservice.audit.backend.service;

import com.microservice.audit.backend.dto.OrderEvent;
import com.microservice.audit.backend.dto.UpdateEvent;
import com.microservice.audit.backend.entities.AuditEvent;
import com.microservice.audit.backend.repository.UpdateEventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UpdateEventService {

    @Autowired
    UpdateEventRepository updateEventRepository;

    public AuditEvent persistUpdateEvent(UpdateEvent updateEvent) {
        return updateEventRepository.save(new AuditEvent(updateEvent.getMessage(), updateEvent.getDateTime()));
    }

    public AuditEvent persistOrderEvent(OrderEvent updateEvent) {
        return updateEventRepository.save(new AuditEvent(updateEvent.getMessage(), updateEvent.getDateTime()));
    }
}
