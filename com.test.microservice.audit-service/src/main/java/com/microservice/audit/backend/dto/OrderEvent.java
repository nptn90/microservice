package com.microservice.audit.backend.dto;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@Data
@EqualsAndHashCode(callSuper=false)
@NoArgsConstructor
public class OrderEvent extends Event {
  private String orderId;
  private Date dateTime;

  @Builder
  public OrderEvent(String message, Date dateTime, String orderId) {
    super(message);
    this.dateTime = dateTime;
    this.orderId = orderId;
  }
}
