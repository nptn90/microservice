package com.microservice.audit.backend.service;

import org.springframework.kafka.support.serializer.JsonSerializer;

import com.microservice.audit.backend.dto.Event;

public class CustomJsonSerializer<T extends Event> extends JsonSerializer<T> {
	//Do nothing
}
