package com.microservice.audit.backend.service;

import com.microservice.audit.backend.common.StringConstant;
import com.microservice.audit.backend.dto.OrderEvent;
import com.microservice.audit.backend.dto.UpdateEvent;
import com.microservice.audit.backend.repository.UpdateEventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class EventListener {

	@Autowired
	UpdateEventService updateEventService;

	@KafkaListener(topics = StringConstant.UPDATE_PRODUCT, groupId = StringConstant.PRODUCT, containerFactory = "productKafkaListenerContainerFactory")
	public void ListenUpdateEvent(UpdateEvent event) {
		System.out.println("Update success" + event);
		updateEventService.persistUpdateEvent(event);
	}

	@KafkaListener(topics = StringConstant.ORDER_PRODUCT, groupId = StringConstant.ORDER, containerFactory = "orderKafkaListenerContainerFactory")
	public void ListenOrderEvent(OrderEvent event) {
		System.out.println("Order success" + event);
		updateEventService.persistOrderEvent(event);
	}

}
