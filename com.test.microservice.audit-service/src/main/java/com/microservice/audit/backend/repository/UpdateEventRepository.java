package com.microservice.audit.backend.repository;

import com.microservice.audit.backend.entities.AuditEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UpdateEventRepository extends JpaRepository<AuditEvent, String> {
}
