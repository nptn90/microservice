package com.microservice.audit.backend.dto;

import java.util.Date;

import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Component
@Data
@EqualsAndHashCode(callSuper=false)
@AllArgsConstructor
@NoArgsConstructor
public class UpdateEvent extends Event {
	private Date dateTime;

	@Builder
	public UpdateEvent(String message, Date dateTime) {
		super(message);
		this.dateTime = dateTime;
	}


}
