package com.microservice.audit.backend.entities;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity(name = "audit")
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class AuditEvent extends BaseEntity {

  @Column(name = "event_name")
  private String eventName;

  @Column(name = "event_time")
  private Date eventTime;
}
