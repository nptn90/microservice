package com.microservice.productservice.backend.controller;

import java.util.List;
import java.util.Map;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.microservice.productservice.backend.entity.ProductEntity;
import com.microservice.productservice.backend.service.ProductService;

import lombok.extern.slf4j.Slf4j;

@RestController(value = "/product")
@Slf4j
public class ProductController {
	
	@Autowired
	ProductService productSerivce;
	
	@GetMapping(value = "/health-check")
	public ResponseEntity<String> checkHealth() {
		return ResponseEntity.ok("Service is up");
	}
	
	@GetMapping(value = "/filter")
	public ResponseEntity<List<ProductEntity>> filterProduct(@RequestParam(required = true) Map<String, String> filterMap) {
		List<ProductEntity> filterResult = productSerivce.findProductsBycriteria(filterMap);
		return ResponseEntity.ok(filterResult);
	}
	
	@PutMapping(value = "/update")
	public ResponseEntity<String> update(@Validated @RequestBody ProductEntity product) {
		try {
			productSerivce.doPersistProduct(product);
			return ResponseEntity.status(200).body("Update successful");
		} catch (Exception e) {
			log.error("Update product fail", e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Update failed");
		}
	}
}
