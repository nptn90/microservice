package com.microservice.productservice.backend.service;

import com.microservice.productservice.backend.proto.OrderProductRequest;
import com.microservice.productservice.backend.proto.OrderProductResponse;
import com.microservice.productservice.backend.proto.OrderProductServiceGrpc.OrderProductServiceImplBase;
import io.grpc.stub.StreamObserver;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import net.devh.boot.grpc.server.service.GrpcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.microservice.productservice.backend.entity.ProductEntity;
import com.microservice.productservice.backend.entity.UpdateEvent;
import com.microservice.productservice.backend.mapper.ProductMapper;
import com.microservice.productservice.backend.repository.ProductRepository;

@Service
@GrpcService
public class ProductService extends OrderProductServiceImplBase {

	@Autowired
	ProductRepository productRepo;

	@Autowired
	EventPublisher eventPublisher;

	public List<ProductEntity> findProductsBycriteria(Map<String, String> filterMap) {
		String priceLesserThan = filterMap.getOrDefault("priceLesserThan", "0");
		String priceGreaterThan = filterMap.getOrDefault("priceGreaterThan", "0");
		String productName = filterMap.getOrDefault("name", null);
		String productBrand = filterMap.getOrDefault("brand", null);
		Specification<ProductEntity> specification = Specification
				.where(
						priceValue(Integer.parseInt(priceLesserThan), Integer.parseInt(priceGreaterThan))
				)
				.and(productName(productName))
				.and(productBrand(productBrand));
		return productRepo.findAll(specification);
	}

	public ProductEntity doPersistProduct(ProductEntity product) throws Exception {
		Optional<ProductEntity> oldProduct = productRepo.findById(product.getId());
		if (oldProduct.isPresent()) {
			ProductEntity productPersist = oldProduct.get();
			ProductMapper.MapOldProductWithNewProduct(productPersist, product);
			eventPublisher.publishEvent(UpdateEvent.builder().dateTime(new Date()).message("Update Success").build());
			return productRepo.save(productPersist);
		} else {
			throw new Exception("Cannot persist product");
		}
	}

	@Override
	public void order(OrderProductRequest request, StreamObserver<OrderProductResponse> responseObserver) {
		String messageResponse = productRepo.findById(request.getProductId())
				.map(product -> orderProduct(product, request.getQuantity()))
				.orElseGet(() -> "Order failed");
		OrderProductResponse response = OrderProductResponse.newBuilder()
				.setMessage(messageResponse)
				.build();
		responseObserver.onNext(response);
		responseObserver.onCompleted();
	}

	private String orderProduct(ProductEntity product, int quantity) {
		product.setQuantity(product.getQuantity() - quantity);
		productRepo.save(product);
		return "Order success";
	}

	static Specification<ProductEntity> priceValue(int priceLesserThan, int priceGreaterThan) {
		return (root, query, builder) -> builder.between(root.get("price"), priceGreaterThan, priceLesserThan);
	}

	static Specification<ProductEntity> productName(String productName) {
		return productName == null ? null
				: (root, query, builder) -> builder.like(root.get("name"), '%' + productName + '%');
	}

	static Specification<ProductEntity> productBrand(String productBrand) {
		return productBrand == null ? null
				: (root, query, builder) -> builder.like(root.get("brand"), '%' + productBrand + '%');
	}
}
