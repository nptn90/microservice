package com.microservice.productservice.backend.service;

import org.springframework.kafka.support.serializer.JsonSerializer;

import com.microservice.productservice.backend.entity.Event;

public class CustomJsonSerializer<T extends Event> extends JsonSerializer<T> {
	//Do nothing
}
