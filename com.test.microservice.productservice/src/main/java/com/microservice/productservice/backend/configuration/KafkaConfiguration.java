package com.microservice.productservice.backend.configuration;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

import com.microservice.productservice.backend.entity.Event;
import com.microservice.productservice.backend.service.CustomJsonSerializer;

@Configuration
public class KafkaConfiguration {
	@Bean
	   public <T extends Event> ProducerFactory<String, T> producerFactory() {
	      Map<String, Object> configProps = new HashMap<>();
	      configProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
	      configProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
	      configProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, CustomJsonSerializer.class);
	      return new DefaultKafkaProducerFactory<>(configProps);
	   }
	   @Bean
	   public <T extends Event> KafkaTemplate<String, T> kafkaTemplate() {
	      return new KafkaTemplate<>(producerFactory());
	   }
}
