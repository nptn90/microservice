package com.microservice.productservice.backend.entity;

import javax.persistence.Column;
import javax.persistence.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity(name = "product")
@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper=false)
public class ProductEntity extends BaseEntity {
	
	@Column(name = "product_name")
	private String name;
	
	@Column(name = "price")
	private int price;
	
	@Column(name = "brand")
	private String brand;
	
	@Column(name = "color")
	private String color;

	@Column(name = "quantity")
	private int quantity;
}
