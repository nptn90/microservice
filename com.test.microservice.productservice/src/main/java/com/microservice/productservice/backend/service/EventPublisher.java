package com.microservice.productservice.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.microservice.productservice.backend.common.StringConstanst;
import com.microservice.productservice.backend.entity.Event;

@Service
public class EventPublisher {
	
	@Autowired
	private KafkaTemplate<String, Event> kafkaTemplate;
	
	public <T extends Event> void publishEvent (T event) {
		kafkaTemplate.send(StringConstanst.UPDATE_PRODUCT, event);
	}
}
