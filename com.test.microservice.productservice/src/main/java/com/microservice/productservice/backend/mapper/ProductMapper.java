package com.microservice.productservice.backend.mapper;

import com.microservice.productservice.backend.entity.ProductEntity;

public final class ProductMapper {
	public static void MapOldProductWithNewProduct(ProductEntity oldProduct, ProductEntity newProduct) {
		if(newProduct.getBrand() != null) {
			oldProduct.setBrand(newProduct.getBrand());
		}
		
		if(newProduct.getColor()!= null) {
			oldProduct.setColor(newProduct.getColor());
		}
		
		if(newProduct.getPrice() > 0) {
			oldProduct.setPrice(newProduct.getPrice());
		}
		
		if(newProduct.getName() != null) {
			oldProduct.setName(newProduct.getName());
		}
	}
}
